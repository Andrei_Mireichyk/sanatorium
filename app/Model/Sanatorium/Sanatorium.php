<?php

namespace App\Model\Sanatorium;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed $doctors
 * @property mixed $infrastructure
 * @property \Carbon\Carbon $created_at
 * @property int $id
 * @property \Carbon\Carbon $updated_at
 * @property mixed $procedures
 * @property mixed $cabinets
 * @property mixed $medical_profiles
 * @property mixed $buildings
 * @property mixed $complex_programs
 * @property mixed $paid_procedures
 * @property mixed $free_procedures
 * @property mixed $medical_cabinets
 * @property mixed $diagnostic_cabinets
 * @property mixed $functional_diagnostics
 * @property mixed $laboratory_diagnostics
 * @property mixed $rent
 */
class Sanatorium extends Model
{
    use Sluggable;

    protected $table = 'sanatoriums';

    protected $guarded = [];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function medicalProfiles()
    {
        return $this->belongsToMany(MedicalProfile::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function infrastructure()
    {
        return $this->belongsToMany(Infrastructure::class)
            ->using(InfrastructureSanatorium::class)
            ->withPivot(['description', 'payment_id'])
            ->as('description');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function doctors()
    {
        return $this->belongsToMany(Doctor::class)
            ->using(DoctorSanatorium::class)
            ->withPivot(['amount'])
            ->as('description');
    }

    /**
     * лечебная база
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function cabinets()
    {
        return $this->belongsToMany(Cabinet::class)
            ->using(CabinetSanatorium::class)
            ->withPivot(['description', 'type'])
            ->as('description');
    }

    /**
     * медицинские услуги
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function procedures()
    {
        return $this->belongsToMany(Cabinet::class)
            ->using(MedicalProcedureSanatorium::class)
            ->withPivot(['price', 'description', 'type'])
            ->as('description');
    }

    /**
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function diagnostics()
    {
        return $this->belongsToMany(MedicalDiagnostic::class)
            ->using(MedicalDiagnosticSanatorium::class)
            ->withPivot(['description', 'type'])
            ->as('description');
    }


    /**
     * Дополнительные услуги
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function rent()
    {
        return $this->belongsToMany(Rent::class)
            ->using(RentSanatorium::class)
            ->withPivot(['description', 'price', 'amount_hour'])
            ->as('description');
    }

    /**
     * Постройки
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function buildings()
    {
        return $this->hasMany(Building::class);
    }

    /**
     * Комплексные медицинские программы
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function complexPrograms()
    {
        return $this->hasMany(ComplexProgram::class);
    }

}

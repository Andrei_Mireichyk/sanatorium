<?php

namespace App\Model\Sanatorium;

use Illuminate\Database\Eloquent\Model;

class MedicalDiagnosticSanatorium extends Model
{
    protected $table = 'medical_diagnostic_sanatorium';
}

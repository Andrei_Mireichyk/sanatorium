<?php

namespace App\Model\Sanatorium;

use Illuminate\Database\Eloquent\Model;

class CabinetSanatorium extends Model
{
    protected $table = 'cabinet_sanatorium';
}

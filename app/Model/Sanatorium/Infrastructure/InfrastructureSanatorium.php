<?php
/**
 * Created by PhpStorm.
 * User: 3
 * Date: 23.08.2019
 * Time: 12:30
 */

namespace App\Model\Sanatorium;

use Illuminate\Database\Eloquent\Relations\Pivot;

/**
 * @property \Carbon\Carbon $created_at
 * @property int $id
 * @property \Carbon\Carbon $updated_at
 * @property mixed $payment
 */
class InfrastructureSanatorium extends Pivot
{
    protected $table = 'infrastructure_sanatorium';


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function payment(){
        return $this->hasOne(Payment::class);
    }
}
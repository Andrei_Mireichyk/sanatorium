<?php

namespace App\Model\Sanatorium;

use Illuminate\Database\Eloquent\Relations\Pivot;

class RentSanatorium extends Pivot
{
    protected $table = 'rent_sanatorium';
}

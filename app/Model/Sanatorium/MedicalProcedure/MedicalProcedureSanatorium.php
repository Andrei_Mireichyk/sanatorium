<?php

namespace App\Model\Sanatorium;

use Illuminate\Database\Eloquent\Model;

class MedicalProcedureSanatorium extends Model
{
    protected $table = 'medical_procedure_sanatorium';
}

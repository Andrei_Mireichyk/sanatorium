<?php

namespace App\Model\Parser;

use function foo\func;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Symfony\Component\DomCrawler\Crawler;

/**
 * @property object $settings
 * @property \Carbon\Carbon $created_at
 * @property int $id
 * @property \Carbon\Carbon $updated_at
 */
class Parser extends Model
{

    public function run()
    {
        $crawler_all = new Crawler($this->getContent('https://pda.sanatorii.by/?calendar=detail'));

        $links = $this->getListUri($crawler_all);

        echo "Total objects " . count($links) . PHP_EOL;

        foreach ($links as $key => $link) {

            echo "\e[0;30;43m[$key] Start $link\e[0m" . PHP_EOL;

            $this->handler($link);
        }
    }

    public function handler(string $link)
    {
        $crawler_main = new Crawler($this->getContent($link . '=Main'));

        $crawler_heal = new Crawler($this->getContent($link . '=HealBase'));

        $crawler_paid_procedures = new Crawler($this->getContent($link . '=Meditcinskie_uslugi&paid'));

        $crawler_free_procedures = new Crawler($this->getContent($link . '=Meditcinskie_uslugi&free'));

        $crawler_infrastructure = new Crawler($this->getContent($link . '=Infra'));

        $crawler_address = new Crawler($this->getContent($link . '=Address'));

        $crawler_photo = new Crawler($this->getContent($link . '=Foto'));

        $crawler_food = new Crawler($this->getContent($link . '=Food'));


        $sanatorium['title'] = $this->getSanatoriumTitle($crawler_main);

        $sanatorium['description'] = $this->getSanatoriumDesc($crawler_main);

        $sanatorium['rooms'] = $this->getRooms($crawler_main);

        $sanatorium['medical_profiles'] = $this->getMedicalProfiles($crawler_heal);

        $sanatorium['doctors'] = $this->getDoctors($crawler_heal);

        $sanatorium['complex_program'] = $this->getComplexProgram($crawler_heal);

        $sanatorium['medical_cabinet'] = $this->getMedicalCabinet($crawler_heal);

        $sanatorium['diagnostic_cabinet'] = $this->getDiagnosticCabinet($crawler_heal);

        $sanatorium['functional_diagnostic'] = $this->getFunctionalDiagnostic($crawler_heal);

        $sanatorium['laboratory_diagnostic'] = $this->getLaboratoryDiagnostic($crawler_heal);


        $sanatorium['paid_procedures'] = $this->getProcedures($crawler_paid_procedures);

        $sanatorium['free_procedures'] = $this->getProcedures($crawler_free_procedures);


        $sanatorium['infrastructure'] = $this->getInfrastructure($crawler_infrastructure);

        $sanatorium['nearby_infrastructure'] = $this->getNearbyInfrastructure($crawler_infrastructure);

        $sanatorium['rent'] = $this->getRent($crawler_infrastructure);

        $sanatorium['leisure'] = $this->getLeisure($crawler_infrastructure);

        $sanatorium['excursion'] = $this->getExcursion($crawler_infrastructure);


        $sanatorium['travel_car'] = $this->getTravelCar($crawler_address);

        $sanatorium['travel_public'] = $this->getTravelPublic($crawler_address);

        $sanatorium['address'] = $this->getAddress($crawler_address);

        $sanatorium['img_links'] = $this->getImagesLink($crawler_photo);

        $sanatorium['food'] = $this->getFood($crawler_food);

        //Save result
        SourceSanatoriiBy::updateOrCreate(
            ['uri' => $link],
            ['data' => json_encode($sanatorium)]
        );
    }

    protected function getListUri(Crawler $crawler): array
    {
        $xpath = "//h3[@class='tour_title']//a";

        $nodes = $crawler->filterXPath($xpath);

        return $nodes->each(function (Crawler $node) {
            return preg_replace('/#.+$/', '', $node->attr('href'));
        });
    }

    protected function getSanatoriumAlias($link): string
    {
        preg_match('/\?(.+)#/', $link, $matches);

        return end($matches);
    }

    protected function getSanatoriumTitle(Crawler $crawler)
    {
        $xpath = "//h1[@class='title_h1 title_object']";

        return $crawler->filterXPath($xpath)->text('');
    }

    protected function getSanatoriumDesc(Crawler $crawler)
    {
        $selector = '#commonInfo~div'; //TODO settings

        $text = $crawler->filter($selector)->text('');

        return trim(preg_replace('/\s+/', ' ', $text));
    }

    protected function getRooms(Crawler $crawler)
    {
        $xpath = "//*[@data-block='room']";

        $nodes = $crawler->filterXPath($xpath);

        return $nodes->each(function (Crawler $node) {

            $title = $node->filterXPath("//h2[@class='page_head']")->text();

            $size = $node->filterXPath("//img")->count();

            $description = $node->filterXPath("//p[@class='name_price']")->text();

            $options = $node->filterXPath("//ul[@class='korp_include']")->text();

            $options = $this->removeLinebaks($options);

            $options = explode(', ', $options);

            $images = $node->filterXPath("//li[@class='room_item']//a");

            $images = $images->each(function (Crawler $node) {
                return $node->attr('data-background-url');
            });

            $price = $node->filterXPath("//div[@data-block='cost_details']//h4");

            $price = $price->each(function (Crawler $node) {
                return trim($node->text(''));
            });

            return [
                'title' => $title,
                'size' => $size,
                'description' => $description,
                'options' => $options,
                'images' => $images,
                'price' => $price,
            ];
        });
    }

    protected function getMedicalProfiles(Crawler $crawler)
    {
        $xpath = '//*[contains(@id, "Medicinskii_profil_")]//li//a';

        $nodes = $crawler->filterXPath($xpath);

        return $nodes->each(function (Crawler $node) {
            return $node->text('');
        });
    }

    protected function getDoctors(Crawler $crawler): array
    {
        $xpath = '//*[contains(@id, "Vrachi_")]//ul/li';

        $nodes = $crawler->filterXPath($xpath);

        return $nodes->each(function (Crawler $node) {

            $title = $node->filterXPath('//a')->text('');

            $amount = $node->filterXPath('//span')->text('');

            return [
                'title' => $this->removeLinebaks($title),
                'amount' => $this->getOnlyNum($amount),
            ];

        });
    }

    protected function getComplexProgram(Crawler $crawler): array
    {
        $xpath = '//*[contains(@id, "Medicinskie_programmy_sanatorii")]//ul/li';

        $nodes = $crawler->filterXPath($xpath);

        return $nodes->each(function (Crawler $node) {
            $text = $node->filterXPath('//a')->text('');
            return $this->removeLinebaks($text);
        });
    }

    protected function getProcedures(Crawler $crawler): array
    {
        $xpath = "//main[@class='main_content']//ol//li//a";

        $nodes = $crawler->filterXPath($xpath);

        return $nodes->each(function (Crawler $node) {
            return $this->removeLinebaks($node->text(''));
        });
    }

    protected function getMedicalCabinet(Crawler $crawler): array
    {
        $xpath = "//h2[text()='лечебная база']/following::ul[1]//li";

        $nodes = $crawler->filterXPath($xpath);

        return $nodes->each(function (Crawler $node) {
            $title = $node->filterXPath('//div/h4/a')->text('');
            $description = $node->filterXPath('//div/div/div')->text('');

            return [
                'title' => $this->removeLinebaks($title),
                'description' => $this->removeLinebaks($description),
            ];

        });
    }

    protected function getDiagnosticCabinet(Crawler $crawler): array
    {
        $xpath = "//h2[text()='медицинская диагностическая база']/following::ul[1]//li";

        $nodes = $crawler->filterXPath($xpath);

        return $nodes->each(function (Crawler $node) {
            $title = $node->filterXPath('//div/h4/a')->text('');
            $description = $node->filterXPath('//div/div/div')->text('');

            return [
                'title' => $this->removeLinebaks($title),
                'description' => $this->removeLinebaks($description),
            ];

        });

    }

    protected function getFunctionalDiagnostic(Crawler $crawler): array
    {
        $xpath = "//h3[text()='методы функциональной диагностики']/following::div[1]//ul//li[not(@hidden)]";

        $nodes = $crawler->filterXPath($xpath);

        return $nodes->each(function (Crawler $node) {

            $title = $node->filterXPath("//a")->text('');

            $description = $node->filterXPath("//span")->text('');

            $description = preg_replace('/[\(\)]/', '', $description);

            return [
                'title' => $this->removeLinebaks($title),
                'description' => $this->removeLinebaks($description),
            ];
        });

    }

    protected function getLaboratoryDiagnostic(Crawler $crawler): array
    {
        $xpath = "//h3[text()='методы лабораторной диагностики']/following::div[1]//ul//li[not(@hidden)]";

        $nodes = $crawler->filterXPath($xpath);

        return $nodes->each(function (Crawler $node) {

            $title = $node->filterXPath("//a")->text('');

            $description = $node->filterXPath("//span")->text('');

            $description = preg_replace('/[\(\)]/', '', $description);

            return [
                'title' => $this->removeLinebaks($title),
                'description' => $this->removeLinebaks($description),
            ];
        });

    }

    protected function getInfrastructure(Crawler $crawler): array
    {
        $xpath = "//ul[@class='places_list infra_list']//li";

        $nodes = $crawler->filterXPath($xpath);

        return $nodes->each(function (Crawler $node) {

            $title = $node->filterXPath("//h3//a")->text('');

            $description = $node->filterXPath("//div//span")->text('');

            $img_src = $node->filterXPath("//div//img")->attr('src');

            return [
                'title' => $this->removeLinebaks($title),
                'description' => $this->removeLinebaks($description),
                'payment' => $this->resolvePayment($img_src),
            ];
        });

    }

    public function getNearbyInfrastructure(Crawler $crawler): array
    {
        $xpath = "//h2[contains(text(), 'Расстояние до ближайших пунктов сервиса')]/following::div[1]//ul//li";

        $nodes = $crawler->filterXPath($xpath);

        return $nodes->each(function (Crawler $node) {
            return $node->text('');
        });
    }

    protected function getRent(Crawler $crawler): array
    {
        $xpath = "//h2[text()='Дополнительные услуги']/following::div[1]//ol//li";

        $nodes = $crawler->filterXPath($xpath);

        return $nodes->each(function (Crawler $node) {

            $title = $node->filterXPath("//a")->text('');

            $price = $node->filterXPath("//nobr")->text('');

            $amount_hour = $node->filterXPath('//*[contains(text(), "/")]')->text('');

            return [
                'title' => $this->removeLinebaks($title),
                'price' => $this->removeLinebaks($price),
                'amount_hour' => $this->getOnlyNum($amount_hour),
            ];
        });

    }

    protected function getLeisure(Crawler $crawler): array
    {
        $xpath = "//h2[text()='Организация досуга']/following::div[1]//ul//li";

        $nodes = $crawler->filterXPath($xpath);

        return $nodes->each(function (Crawler $node) {

            return $this->removeLinebaks($node->text(''));
        });

    }

    protected function getExcursion(Crawler $crawler): array
    {
        $xpath = "//h2[text()='Экскурсии']/following::div[1]//ul//li";

        $nodes = $crawler->filterXPath($xpath);

        return $nodes->each(function (Crawler $node) {

            return $this->removeLinebaks($node->text(''));

        });

    }

    protected function getTravelCar(Crawler $crawler): array
    {

        $xpath = "//div[@class='container']//div[2]//div[1]//ul[1]//li";

        $nodes = $crawler->filterXPath($xpath);

        return $nodes->each(function (Crawler $node) {

            return $this->removeLinebaks($node->text(''));

        });

    }

    protected function getTravelPublic(Crawler $crawler): array
    {
        $xpath = "//h2[contains(text(), 'Проезд на общественном транспорте')]/following::div[1]//ul[1]//li";

        $nodes = $crawler->filterXPath($xpath);

        return $nodes->each(function (Crawler $node) {

            return $this->removeLinebaks($node->text(''));
        });
    }

    protected function getAddress(Crawler $crawler): string
    {

        $xpath = "//h2[contains(text(), 'Адрес — ')]/following::p[1]";

        return $crawler->filterXPath($xpath)->text('');
    }

    protected function getFood(Crawler $crawler): array
    {
        //food description
        $xpath = "//main/div/div[3]/ul[1]/li";

        $nodes = $crawler->filterXPath($xpath);

        $food['desc'] = $nodes->each(function (Crawler $node) {
            return $this->removeLinebaks($node->text(''));
        });

        //food type
        $xpath = "//*[contains(text(), 'тип питания')]/../../following::ul[1]/li[not(table)]";

        $nodes = $crawler->filterXPath($xpath);

        $food['type'] = $nodes->each(function (Crawler $node) {
            return $this->removeLinebaks($node->text(''));
        });

        //food timing
        $xpath = "//table[@class='tab']//tr";

        $nodes = $crawler->filterXPath($xpath);

        $food['timing'] = $nodes->each(function (Crawler $node) {
            return $this->removeLinebaks($node->text(''));
        });

        return $food;
    }

    protected function getImagesLink(Crawler $crawler): array
    {
        $xpath = "//h2[@class='head_contact head_blue']/following::a[@class='photo_link']";

        $nodes = $crawler->filterXPath($xpath);

        if (!$nodes->count()) return [];

        $images = $nodes->each(function (Crawler $node) {

            $namespace = Str::slug($this->resolveImgGroupName($node->attr('title')), '_');

            return [
                'title' => $this->resolveImgGroupName($node->attr('title')),
                'href' => $node->attr('href'),
                'namespace' => $namespace
            ];
        });

        $images = collect($images);

        return $images->groupBy('namespace')->toArray();


    }

    protected function removeLinebaks(string $str): string
    {
        return trim(preg_replace('/\s+/', ' ', $str));
    }

    protected function removeHashOpts(string $link): string
    {
        return preg_replace('/#.+$/', '', $link);
    }

    protected function getOnlyNum(string $str): string
    {
        preg_match('/[0-9]+/', $str, $matches);
        return end($matches);
    }

    protected function resolvePayment(string $str): string
    {
        switch ($str) {
            case 'img/point2.gif':
                return 'включено в стоимость путёвки';
            case 'img/point3.gif':
                return 'включено в стоимость путёвки, сверх включенного в стоимость возможно за дополнительную плату';
            case 'img/point1.gif':
                return 'за дополнительную плату';
        }
    }

    protected function resolveImgGroupName(string $str): string
    {
        return preg_replace('/^(.)+(  )/m', '', $str);
        preg_match('/[А-яё ]+$/', $str, $matches);
    }

    protected function getContent(string $link): string
    {
        try {

            sleep(2);

            $client = new Client([
                'headers' => [
                    'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36',
                ]
            ]);

            echo "Download...($link)  " . PHP_EOL;

            return $client->request('get', $link)
                ->getBody()
                ->getContents();

        } catch (GuzzleException $e) {
            return $e;
        }
    }
}

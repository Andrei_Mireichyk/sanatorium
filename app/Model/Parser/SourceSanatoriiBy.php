<?php

namespace App\Model\Parser;

use Illuminate\Database\Eloquent\Model;

class SourceSanatoriiBy extends Model
{
    protected $table = 'source_sanatorii_by';

    protected $guarded = [];
}

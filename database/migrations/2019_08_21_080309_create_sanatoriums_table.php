<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSanatoriumsTable extends Migration
{
    /**yjv
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sanatoriums', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->string('slug');
            $table->text('description')->nullable();
            $table->integer('foundation_year')->nullable();
            $table->integer('reconstruction_year')->nullable();
            $table->integer('amount_rooms')->nullable();
            $table->decimal('area', 3, 2)->nullable();
            $table->string('check_in')->nullable();
            $table->string('check_out')->nullable();
            $table->string('address')->nullable();
            $table->integer('country_id')->nullable();
            $table->integer('city_id')->nullable();
            $table->integer('region_id')->nullable();
            $table->string('coordinates')->nullable();
            $table->string('schedule_admin')->nullable();
            $table->string('schedule_registry')->nullable();
            $table->string('schedule_nurse')->nullable();
            $table->string('pay_card')->nullable();
            $table->text('excursion')->nullable();
            $table->text('leisure')->nullable();
            $table->text('travel_car')->nullable();
            $table->text('travel_public')->nullable();
            $table->text('nearby_infrastructure')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sanatoriums');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRentSanatoriumTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rent_sanatorium', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('rent_id');
            $table->integer('sanatorium_id');
            $table->string('description');
            $table->decimal('price');
            $table->decimal('amount_hour');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rent_sanatorium');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMedicalProcedureSanatoriumTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medical_procedure_sanatorium', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('sanatorium_id');
            $table->integer('medical_procedure_id');
            $table->string('description');
            $table->enum('type', ['paid', 'free']);
            $table->decimal('price');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('medical_procedure_sanatorium');
    }
}

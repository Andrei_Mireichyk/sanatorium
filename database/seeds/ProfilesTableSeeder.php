<?php

use Illuminate\Database\Seeder;
use App\Model\Sanatorium\MedicalProfile;

class ProfilesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->getMockData() as $title){
            $profile = new MedicalProfile();
            $profile->setAttribute('title', $title);
            $profile->save();
        }
    }

    public function getMockData()
    {
        return [
            'Женских половых органов',
            'Кожи',
            'Костно-мышечной системы',
            'Мочеполовой системы',
            'Нервной системы',
            'Обмена веществ',
            'Опорно-двигательного аппарата',
            'Органов дыхания',
            'Органов пищеварения',
            'Сердечно-сосудистой системы',
            'Эндокринной системы',
            'Общетерапевтический',
            'Онкология',
        ];
    }
}

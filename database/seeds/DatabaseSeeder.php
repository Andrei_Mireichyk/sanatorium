<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(PaymentsTableSeeder::class);
        $this->call(ProfilesTableSeeder::class);
        $this->call(MedicalProceduresTableSeeder::class);
        $this->call(DoctorsTableSeeder::class);
        $this->call(SanatoriumsTableSeeder::class);
    }
}

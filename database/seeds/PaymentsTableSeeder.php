<?php

use \App\Model\Sanatorium\Payment;
use Illuminate\Database\Seeder;

class PaymentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->getMockData() as $title) {
            $doctor = new Payment();
            $doctor->setAttribute('title', $title);
            $doctor->save();
        }
    }

    protected function getMockData()
    {
        return [
            'включено в стоимость путёвки',
            'включено в стоимость путёвки, сверх включенного в стоимость возможно за дополнительную плату',
            'за дополнительную плату'
        ];
    }
}

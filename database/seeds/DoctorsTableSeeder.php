<?php

use Illuminate\Database\Seeder;
use \App\Model\Sanatorium\Doctor;

class DoctorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        foreach ($this->getMockData() as $title) {
            $doctor = new Doctor();
            $doctor->setAttribute('title', $title);
            $doctor->save();
        }
    }

    public function getMockData()
    {
        return [
            'акушер-гинеколог',
            'аллерголог',
            'гастроэнтеролог',
            'гинеколог',
            'гомеопат',
            'дерматолог',
            'диетолог',
            'иглорефлексотерапевт',
            'инструктор ЛФК',
            'кардиолог',
            'косметолог',
            'лаборант',
            'логопед',
            'мануальный терапевт',
            'МРТ',
            'невролог',
            'нефролог',
            'озонотерапевт',
            'онколог',
            'ортопед',
            'ортопед-травматолог',
            'оториноларинголог',
            'офтальмолог',
            'педиатр',
            'проктолог',
            'психолог',
            'психотерапевт',
            'пульмонолог',
            'реабилитолог',
            'ревматолог',
            'рентгенолог',
            'рефлексотерапевт',
            'стоматолог',
            'терапевт',
            'УЗИ-диагностики',
            'уролог',
            'физиотерапевт',
            'флеболог',
            'функциональной диагностики',
            'хирург',
            'эндокринолог',
            'эндоскопист',
        ];
    }
}
